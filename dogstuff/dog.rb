class Dog
  #attr_reader :name

  def initialize(name, will_wag = true, will_shake = true)
    @name = name
    @has_wag = will_wag
    @has_shake = will_shake
  end

  def name
    @name
  end

  def wag
    if @has_wag
      "wag wag wag wag"
    else
      @has_wag
    end
  end

  def shake
    @has_shake
  end

end
