# the ./ is needed to let ruby know to look in this directory
# since i haven't installed the dogpark and other classes 
# as gems like when i had you gem install wirble
# and your could just require 'wirble'
require './dogpark2'
require './dog'

@dogs = []

@dogparks = []
@dogparks.push(DogPark.new("downtown",12,4))
@dogparks.push(DogPark.new("uptown",200,200))
@dogparks.push(DogPark.new("ghetto",0,0))


def init
  @dogs.push(Dog.new("Bobo"))
  @dogs.push(Dog.new("Bebop",false))
  @dogs.push(Dog.new("Bilbox",false,false))
end

def do_stuff
  for dogpark in @dogparks
    puts "At the #{dogpark.name} park!"
    @dogs.each do |dog|
      dogpark.walk(dog)
      dogpark.wash(dog)
    end
  end
end

init
do_stuff
