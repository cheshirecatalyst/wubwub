# the ./ is needed to let ruby know to look in this directory
# since i haven't installed the dogpark and other classes 
# as gems like when i had you gem install wirble
# and your could just require 'wirble'
require './dogpark'
require './dog'

@dogs = []
@downtown_dogpark = DogPark.new(12,4)
@uptown_dogpark = DogPark.new(200,200)
@ghetto_dogpark = DogPark.new(0,0)


def init
  @dogs.push(Dog.new("Bobo"))
  @dogs.push(Dog.new("Bebop",false))
  @dogs.push(Dog.new("Bilbox",false,false))
end

def do_stuff
  #one way to do it
  for dog in @dogs
    @downtown_dogpark.walk(dog)
    @downtown_dogpark.wash(dog)
    puts dog.inspect
  end
  # a 'ruby' way
  @dogs.each { |dog| @uptown_dogpark.walk(dog)}
  @dogs.each { |dog| @uptown_dogpark.wash(dog)}
  # a 'stupid' way to do it
  @ghetto_dogpark.walk(@dogs[0])
  @ghetto_dogpark.wash(@dogs[0])
  @ghetto_dogpark.walk(@dogs[1])
  @ghetto_dogpark.wash(@dogs[1])
  @ghetto_dogpark.walk(@dogs[2])
  @ghetto_dogpark.wash(@dogs[2])
end

init
do_stuff
