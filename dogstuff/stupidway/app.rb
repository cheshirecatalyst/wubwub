class Dog
  attr_reader :name

  def initialize(name, will_wag = true, will_shake = true)
    @name = name
    @has_wag = will_wag
    @has_shake = will_shake
  end

  def wag
    if @has_wag
      "wag wag wag wag"
    else
      @has_wag
    end
  end

  def shake
    @has_shake
  end

end

class DogPark
  def initialize(num_of_washers,num_of_walkers)
    @washers = num_of_washers
    @walkers = num_of_washers
  end

  def walk(dog)
    if @walkers != 0
      puts "Walking #{dog.name}!"
      if dog.wag
        dog.wag
      else
        puts "#{dog.name} is a gimp"
      end
    else
      puts "You need walkers to walk the dogs fool!"
    end
  end

  def wash(dog)
    if @washers != 0
      puts "Washing #{dog.name}!"
      if dog.shake
        puts "#{dog.name} shook itself dry"
      else
        puts "i had to towel dry #{dog.name}"
      end
    else
      puts "You need washers to wash the dogs fool!"
    end
  end

end
# the above is basically what the result of calling import in
# in the original app does
# begin real code
@dogs = []
@downtown_dogpark = DogPark.new(12,4)
@uptown_dogpark = DogPark.new(200,200)
@ghetto_dogpark = DogPark.new(0,0)


def init
  @dogs.push(Dog.new("Bobo"))
  @dogs.push(Dog.new("Bebop",false))
  @dogs.push(Dog.new("Bilbox",false,false))
end

def do_stuff
  #one way to do it
  for dog in @dogs
    @downtown_dogpark.walk(dog)
    @downtown_dogpark.wash(dog)
    puts dog.inspect
  end
  # a 'ruby' way
  @dogs.each { |dog| @uptown_dogpark.walk(dog)}
  @dogs.each { |dog| @uptown_dogpark.wash(dog)}
  # a 'stupid' way to do it
  @ghetto_dogpark.walk(@dogs[0])
  @ghetto_dogpark.wash(@dogs[0])
  @ghetto_dogpark.walk(@dogs[1])
  @ghetto_dogpark.wash(@dogs[1])
  @ghetto_dogpark.walk(@dogs[2])
  @ghetto_dogpark.wash(@dogs[2])
end

init
do_stuff