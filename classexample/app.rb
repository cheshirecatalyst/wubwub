#!/usr/bin/env ruby -wKU
require './bag'
require './betterbag'

puts "<- First bag demo ->"
myBag = Bag.new(['carrot','Apple','strawberry'])
myBag.proof_it_is_holding_the_items
# but we can't see teh contents directly from out here
begin
  somestuff = myBag.contents
rescue Exception => error
  puts "No outside access without an attr_reader"
  puts error.inspect
end
puts "<-First bag errors ->"
begin
  anotherBag = Bag.new('Blueberry')
rescue Exception => error
  puts error.inspect
end
puts "<- Better bag demo ->"
mySecondBag = BetterBag.new(['carrot','Apple','strawberry'])
puts "Using the class method proof_it_is_holding_the_items"
mySecondBag.proof_it_is_holding_the_items
puts "Using the . or dot notation"
# because of attr_reader i'm able to read the contents variable of the
# BetterBag class and not get an error
stuff = mySecondBag.contents
stuff.each do |item|
  # note that the method the module provided is even accesible out here..neat
  article = mySecondBag.correct_article(item)
  puts "Hai I'm #{article} #{item} in a bag"
end
puts "<-Better bag errors....or really lack thereof->"
begin
  secondBagTest = BetterBag.new('Blueberry')
  secondBagTest.proof_it_is_holding_the_items
rescue Exception => error
  puts "You'll never see this"
  puts error.inspect
end
