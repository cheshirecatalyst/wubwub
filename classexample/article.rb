module Article

  def correct_article(t)
    article = ""
    if t.downcase.start_with?("a","e","i","o","u")
      article = "an"
    else
      article = "a"
    end
    article
  end

end