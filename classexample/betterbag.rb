# here we get ruby to load the code in article.rb
require "./article"
class BetterBag
  """
  This is a better bag, in some ways, for one i'll be using the a modulw 
  and we, will use an attr_reader, that the code in app.rb will demonstrate 
  using via the '.'' notation, and we will handle being passed a single 
  item gracefully
  """
  # here we use attr_reader to allow outside code to look at
  # @contents in a direct manner
  attr_reader :contents
  # here we 'mixin' the code that we required
  include Article
  #regular init, but now we handle single items like a boss
  def initialize(items)
    if items.is_a? Array
      @contents = items
    else
      # bam we place it inside an array...like a boss, or gracefully
      # whichever you prefer to call it
      @contents = [items]
    end
  end

  # there is no reason for this method, since we have the attr_reader
  # letting the outside look at the array, but this is here to 
  # demonstrate modules, and for 'convenience'

  def proof_it_is_holding_the_items
    @contents.each do |item|
      # now we are calling correct_article from the module....neat
      article = correct_article(item)
      puts "Hai I'm #{article} #{item} in a bag"
    end
  end

end