class Bag
  """
  This is our standard bag, it has no way of reading or changing
  the items once we've put them in to it, so it is useless but to
  prove that it works i'll add a method to be called from the 
  outside that will puts the contents so we can look
  """

  def initialize(items)
    # not needed, but it's good to get int the habit
    # of making sure you are getting what you need
    # so you can throw a more appropriate error
    if items.is_a? Array
      # so if it's an array and we're good, set it
      @contents = items
    else
      # otherwise...trhow a bitchin custom error
      raise 'WTF why would you call me with something other than an array?'
    end
  end

  def proof_it_is_holding_the_items
    article = ""
    @contents.each do |item|
      # Sorry i got a little fancy here
      # but it will also help me demonstrate modules
      # in my next bag example
      if item.downcase.start_with?("a","e","i","o","u")
        article = "an"
      else
        article = "a"
      end
      puts "Hai I'm #{article} #{item} in a bag"
    end
  end

end